# squeal

A Rust Query Builder.

Create a (safe, correct) Rust structure representing a SQL query and then render it into SQL itself.

Escape hatches in appropriate places are designed in.

The targeted SQL dialect is PostgreSQL. The Postgres library is  https://docs.rs/postgres/latest/postgres/

A similar library for Go is https://github.com/leporo/sqlf.

# readiness

This code is not ready for use.

Built against Rust 1.75.x.

# license

License: LGPL 3.0 or later.